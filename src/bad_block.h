#ifndef BAD_BLOCK_H
#define BAD_BLOCK_H

#include "common.h"
#include "nand.h"

#define BADBLOCK_POS 0
#define BADBLOCK_LEN 2
#define BBM_MARKER 0xffff
#define GOODBLOCK -1
#define ZERO 0x0000

int16_t badBlockTable[NUM_OF_BLOCKS];
uint16_t nextResBlock;

// Initializes the bad block table by checking all
// blocks that have a bad block indicator in the OOB.
// Table is just an array where the index corresponds
// to block number. -1 indicates block is fine,
// otherwise it indicates the reserved block it's
// linked to.
int16_t *initBBM ( );

// Returns the next availiable block from reserved space
// Will also update the bad block table with the forwarding address
int32_t updateBadBlocks(int32_t blockNum);

// Writes out indicators for each bad block
// Will return the out of bounds size if good or and ERROR_CODE if it failed
int32_t writeBadBlocks( );

// Returns whether or not a block is bad
boolean is_block_bad(uint32_t blockNum);

// Sets a block to a specific status
boolean set_block(uint32_t blockNum, int32_t status);

// Used for testing
int16_t *mock_bbm_init(boolean all_bad);

#endif
