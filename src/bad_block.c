#include "bad_block.h"

// Helper Function
// Retrieves next available block in the reserved area
// Returns -1 in the case no replacement block is found
int32_t nextReservedBlock( )
{
    int32_t freeBlock = OUT_OF_RESERVED_BLOCKS;

    while ((nextResBlock < FIRST_DATA_BLOCK) && (freeBlock == -1)){
        if (badBlockTable[nextResBlock] == GOODBLOCK) {
            freeBlock = nextResBlock;
        }
        ++nextResBlock;
    }

    return freeBlock;
}

// Helper function
// Checks the specified pages in the spare area (OOB)
// for bad block markers. Anything other than 0xff
// at the specified byte indicates a bad block.
// -1 indicates a bounds error
// 0 indicates a bad block, while 1 indicates a good block
int32_t checkBlock(int32_t blockNum, uint16_t* bbm_bytes)
{
    int32_t ec;
    uint16_t ob[OOB_SIZE/2];
    void* oob_buf = ob;
    // Make sure block number is valid
    if (blockNum < 0 || blockNum > NUM_OF_BLOCKS)
        return -1;
    // Return -1 if there is an error
    if((ec = oob_read(blockNum, BADBLOCK_POS, &oob_buf)) < 0)
        return ec;
    // Free memory
    memcpy(bbm_bytes, oob_buf, BADBLOCK_LEN);
    free(oob_buf);
    // If block is good
    if(*bbm_bytes == BBM_MARKER)
        return 1;
    // Otherwise block is bad
    return 0;
}

// Helper function
// Zeroes the bad block indicator in the OOB of the block
int32_t writeBlock(uint32_t blockNum)
{
    uint32_t zero = ZERO;
    return oob_write(blockNum, BADBLOCK_POS, &zero, BADBLOCK_LEN);
}

// Run at system start
// Initializes the bad block table by checking each block
int16_t *initBBM()
{
    nextResBlock = BBM_BLOCKS;
    uint16_t* bbm_bytes;
    uint16_t zero = ZERO;
    bbm_bytes = &zero;

    for(uint32_t block = 0; block < NUM_OF_BLOCKS; ++block) {
        if(!checkBlock(block, bbm_bytes)) {
            updateBadBlocks(block);
        } else {
            badBlockTable[block] = GOODBLOCK;
        }
    }

    return badBlockTable;
}

// If a block has gone bad, this will update the BB table with a new reserved block
// address, unless it is a reserved block that has gone bad
// Returns -1 if block indicated is not in range
int32_t updateBadBlocks(int32_t blockNum)
{
    if(blockNum > NUM_OF_BLOCKS)
      return -1;

    // If block is not in the reserved area then update by pulling a new block
    // using nextReservedBlock
    // Otherwise set the reserved block equal to 0
    if (blockNum < BBM_BLOCKS || blockNum > FIRST_DATA_BLOCK) {
        badBlockTable[blockNum] = nextReservedBlock();
    } else {
        badBlockTable[blockNum] = 0;
    }

    return badBlockTable[blockNum];
}

// Writes out bad block markers to all bad block OOBs
int32_t writeBadBlocks( )
{
    int32_t bytes = 0;

    for(uint32_t block = 0; block < NUM_OF_BLOCKS; ++block) {
        if(badBlockTable[block] != GOODBLOCK) {
            if((bytes = writeBlock(block)) < 0) {
                return bytes;
            }
        }
    }
    // Returns the number of bytes last written
    return bytes;
}

// Checks if block is bad
// Returns -1 on error
boolean is_block_bad(uint32_t blockNum)
{
    if (blockNum > NUM_OF_BLOCKS) {
        return false;
    }
    if (badBlockTable[blockNum] != GOODBLOCK) {
        return false;
    }
    return true;
}

// Updates a block's status
// Returns 1 on success and 0 on failure
boolean set_block(uint32_t blockNum, int32_t status)
{
    if (blockNum > NUM_OF_BLOCKS) {
        return false;
    }

    badBlockTable[blockNum] = status;
    return true;
}
