#ifndef MAPPING_TABLE_H
#define MAPPING_TABLE_H

#include "nand.h"
#include <string.h>

typedef enum {
  false = -1,
  true = 0
} boolean;

#include "bad_block.h"

#define DUMMY_TABLE (-1)
#define DEV_TABLE_SIZE 100
// 33% of space is reserved
#define RESERVED_AREA_BLOCKS (NUM_OF_BLOCKS / 3)
#define PHYS_BLOCK_SIZE sizeof(uint32_t)
#define ERASE_COUNT_SIZE sizeof(uint32_t)
#define VALID_SIZE sizeof(boolean)
#define ENTRY_SIZE (PHYS_BLOCK_SIZE + ERASE_COUNT_SIZE + VALID_SIZE + VALID_SIZE)
#define ENTRIES_PER_PAGE (PAGESIZE / ENTRY_SIZE)
#define MTABLE_PAGES (NUM_OF_BLOCKS / ENTRIES_PER_PAGE) + ((NUM_OF_BLOCKS % ENTRIES_PER_PAGE) > 0 ? 0 : 1)
#define MTABLE_BLOCKS 2 * (MTABLE_PAGES / PAGES_PER_BLOCK) + ((MTABLE_PAGES % PAGES_PER_BLOCK) > 0 ? 0 : 1)
#define BBM_BLOCKS ((RESERVED_AREA_BLOCKS - MTABLE_BLOCKS) / 2)
#define GC_BLOCKS ((RESERVED_AREA_BLOCKS - MTABLE_BLOCKS) / 2)
#define USER_DATA_BLOCKS NUM_OF_BLOCKS - RESERVED_AREA_BLOCKS
// First Non-reserved block in the mapping table
#define FIRST_DATA_BLOCK (MTABLE_BLOCKS + BBM_BLOCKS + GC_BLOCKS)

typedef struct {
    uint32_t phys_block;
    uint32_t erase_count;
    boolean valid;
    boolean dirty;
} table_entry;

typedef struct {
    uint32_t block;
    uint32_t page_offset;
} ftl_addr;

typedef struct {
    table_entry **entries;
    uint32_t size;
} mapping_table;


ftl_addr *logical_to_physical(uint32_t lpn, mapping_table *table);

int flush_table(mapping_table *table, uint32_t ppn, uint32_t eraseblock);

boolean set_valid_bit(mapping_table *table, uint32_t lpn, boolean valid);

boolean set_dirty_bit(mapping_table *table, uint32_t lpn, boolean dirty);

boolean free_table(mapping_table *table);

boolean swap_table_entry(mapping_table *table, uint32_t i, uint32_t j);

table_entry *deserialize_te(void *serialized_entry);

char *serialize_te(table_entry *entry);

char *serialize_te_page(mapping_table *table, uint32_t offset);

mapping_table *init_mapping_table(uint32_t fd, uint32_t table_size);

#endif
