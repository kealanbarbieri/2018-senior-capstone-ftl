
#include "ftl_public.h"
#include "error_correction.h"

/* --------------------ftl_public.h------------------------------------------>|
public facing prototypes for user programs

all of these are blocking calls until the requests have been serviced

Negative return values indicate an error

All sizes must be positive
*/

//initializes global data structures, must be called prior to use of
//other public functions
//
void ftl_init()
{
    extern void * BB_TABLE;
    extern void * M_TABLE;
    //initialize global in memory location of bad block table
    BB_TABLE = (void*)initBBM();

    //initialize global in memory location of mapping table
    //temporary argument, init mapping table will eventually have to
    //read existing table from a predictable constant address.
    M_TABLE = (mapping_table *)init_mapping_table(MTABLE_ADDR, NUM_OF_BLOCKS);

    //set global flag indicating successful intialization
    FTL_INIT = true;

    //add a section generating ERASE_MASK, any others that
    //are dependent on makefile defines
}

void ftl_free()
{
    extern void * BB_TABLE;
    extern void * M_TABLE;

    free_table(M_TABLE);
}

//public facing read function
//address must be the virtual address of data of at least size
//returns size of data read into data buffer or negative error code
//
int32_t ftl_read(uint32_t position, void * data, int32_t size)
{
    return ftl_ioctl(position, data, size, READ);
}

//public facing write function
//buffer must point to a buffer of size
//position must be a valid memory position
//returns number of bytes written
//
int32_t ftl_write(uint32_t position, void * data, int32_t size)
{
    return ftl_ioctl(position, data, size, WRITE);
}

int32_t ftl_ioctl(uint32_t position, void * data, int32_t size, iotype type)
{
    uint32_t op_size, maxsize;
    uint32_t lpn = position / PAGESIZE;
    uint32_t byte_offset = position % PAGESIZE;
    int32_t ec;
    char buf[PAGESIZE];
    void *page_buf = buf;
    ftl_addr * paddr = NULL;

    if(!data){
        return NULL_BUFFER;
    }

    lpn += (RESERVED_AREA_BLOCKS - 1)* PAGES_PER_BLOCK;

    if(!(paddr = logical_to_physical(lpn, M_TABLE))){
        return READ_WRITE_BEYOND_DEV_SIZE;
    }

    maxsize = PAGESIZE - byte_offset;

    if( size > maxsize){
       op_size = maxsize;
    }else{
        op_size = size;
    }

    if( true != ((mapping_table*)M_TABLE)->entries[lpn/PAGES_PER_BLOCK]->valid){
        return INVALID_ADDRESS;
    }

    //Read specific logic
    if(type == READ){
        if((ec = read_page_ecc(&page_buf, paddr->block, paddr->page_offset)) < 0){
            return READ_FAILED;
        }
        if((ec = page_read(paddr->block, paddr->page_offset, &page_buf)) != PAGESIZE){
            if(ec < 0){
                return ec;
            }
            return READ_FAILED;
        }
        memcpy(data, page_buf + byte_offset, op_size);
        free(page_buf);
    //write specific logic
    }else if(type == WRITE){
        if(mark_dirty_oob(paddr) < 0){
            return INVALID_ADDRESS;
        }
        memcpy(page_buf + byte_offset, data, op_size);
        if ((ec = write_page_ecc(paddr->block, paddr->page_offset, (uint8_t *)page_buf)) < 0){
            return READ_FAILED;
        }
        if((ec = page_write(paddr->block, paddr->page_offset, page_buf, PAGESIZE)) != PAGESIZE){
            if(ec < 0){
                return ec;
            }
            return WRITE_FAILED;
        }
        if(true != set_dirty_bit(M_TABLE,lpn, true)){
            return NO_FTL_INIT;
        }
    }

    free(paddr);
    return op_size;
}

//public facing erase function
//address must be the virtual address of data of at least size
//returns size on succesful erase, returns negative error code on failure
//
// ONLY whole blocks are marked for erase
// clean pages from erased blocks are moved to clean blocks
//
int32_t ftl_erase(uint32_t position, int32_t size)
{
    ftl_addr * paddr;
    uint32_t pages, lpn = position / PAGESIZE, cleanpgs = 0;
    int32_t i, j, before_ecount;
    char page_buffer[PAGESIZE];
    boolean found = false, failed = false;
    void* readbuf = page_buffer;

    lpn += (RESERVED_AREA_BLOCKS - 1)* PAGES_PER_BLOCK;

    pages = size / PAGESIZE;

    if(size % PAGESIZE){
        ++pages;
    }

    if(!(paddr = logical_to_physical(lpn, M_TABLE))){
        return READ_WRITE_BEYOND_DEV_SIZE;
    }

    mapping_table * table = (mapping_table*)M_TABLE;

    if(!table){
        return NO_FTL_INIT;
    }

    before_ecount = table->entries[lpn/PAGES_PER_BLOCK]->erase_count;

    table_entry ** temp, **end = table->entries + NUM_OF_BLOCKS, *dest=NULL;

    find_clean_block:
    for(temp = table->entries + (FIRST_DATA_BLOCK); temp < end && found == false; ++temp){
        if(true != (*temp)->dirty && true == (*temp)->valid){
            dest = *temp;
            found = true;
        }
    }

    if(!dest){
        if(failed == true){
            return OUT_OF_RESERVED_BLOCKS;
        }
        failed = true;
        ftl_gc();
        goto find_clean_block;
    }


    for(i = 0; i < paddr->page_offset; ++i){
        page_read(paddr->block, paddr->page_offset + i , (&readbuf));

        page_write(dest->phys_block, paddr->page_offset + i, readbuf, PAGESIZE);
        free(readbuf);
        ++cleanpgs;
    }

    for(j = i + pages; j < PAGES_PER_BLOCK; ++j){
        page_read(paddr->block, j , &readbuf);

        page_write(dest->phys_block, j, readbuf, PAGESIZE);
        free(readbuf);
        ++cleanpgs;
    }

    if(failed== true){
        found = false;
        int32_t newlpn;
        for(i=0; i < NUM_OF_BLOCKS && found == false; ++i){
            if(table->entries[i]->phys_block == paddr->block){
                found=true;
                newlpn = i*PAGES_PER_BLOCK;
            }
        }
        if( found == true && table->entries[i-1]->erase_count == before_ecount){
            set_valid_bit(M_TABLE, newlpn, false);
        }

    }else{
        set_valid_bit(M_TABLE, lpn, false);
    }

    dest->dirty=true;

    swap_table_entry(M_TABLE, lpn/PAGES_PER_BLOCK , (--temp) - table->entries);

    if(j > PAGES_PER_BLOCK){
        return (PAGES_PER_BLOCK - i) * PAGESIZE;
    }

    free(paddr);
    return pages * PAGESIZE;

}

//public facing manual gc function
//prompts garbage collection
//returns 0 on success, -1 on failure
//
int32_t ftl_gc()
{

    if(true != FTL_INIT){
        return NO_FTL_INIT;
    }

    return garbage_collect(M_TABLE);
}

int32_t mark_dirty_oob(ftl_addr* address)
{
    uint8_t buf[OOB_SIZE];
    void * bufptr = buf;
    uint8_t dirty_byte = 0x0;
    int32_t ec;
    ec = oob_read(address->block, address->page_offset, &bufptr);
    if(ec != OOB_SIZE){
        return -1;
    }
    if(!((uint8_t *)bufptr)[DIRTY_BYTE] == dirty_byte){
        free(bufptr);
        bufptr = buf;
        memset(bufptr, 0x0, 1);
        // buf[DIRTY_BYTE] = dirty_byte;
        ec = oob_write(address->block, address->page_offset, bufptr, OOB_SIZE);
        if( ec != OOB_SIZE ){
            return -1;
        }
        return 0;
    }
    return -1;
}
