#ifndef FTL_PUBLIC_H
#define FTL_PUBLIC_H

#include "nand.h"
#include "common.h"
#include "gc.h"

/* --------------------ftl_public.h------------------------------------------>|
public facing prototypes for user programs

all of these are blocking calls until the requests have been serviced

Negative return values indicate an error

All sizes must be positive
*/
//public Init function
//initializes shared data structures - BB table and Mapping Table
//This must be called befor any public facing r/w/e/gc calls are made
//failing to initialize before calls will generate an error state tbd
void ftl_init();

//deallocate global data structures
void ftl_free();

int32_t mark_dirty_oob(ftl_addr* address);
//public facing read function
//address must be the virtual address of data of at least size
//returns a buffer containing data in address -(address + size)
//returns zero on failure
//
int32_t ftl_read(uint32_t position, void * data, int32_t size);

//public facing write function
//buffer must point to a buffer of size
//position must be a valid memory position
//returns number of bytes written
//
int32_t ftl_write(uint32_t position, void * data, int32_t size);

int32_t ftl_ioctl(uint32_t position, void * data, int32_t size, uint32_t type);
//public facing erase function
//address must be the virtual address of data of at least size
//returns size on succesful erase, return zero on failure
//
int32_t ftl_erase(uint32_t position, int32_t size);

//public facing manual gc function
//prompts garbage collection
//returns 1 on success, 0 on failure
//
int32_t ftl_gc();


#endif
