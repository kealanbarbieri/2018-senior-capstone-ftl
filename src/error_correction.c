#include "error_correction.h"

// Calculates parity for bits
// Returns parity of bits specified by the mask
static uint8_t calc_bitwise_parity(uint8_t val, uint8_t mask)
{
    uint8_t result = 0, byte_mask;
    int32_t i;

    byte_mask = mask;

    for (i = 0; i < 8; ++i) {
        if ((byte_mask & 0x1) != 0) {
            result ^= (val & 1);
        }
        byte_mask >>= 1;
        val >>= 1;
    }

    return result & 0x1;
}

// Calculates byte-wise parity for entire data chunk
// Div size how the data is being divided for processing
static uint8_t calc_row_parity_bits(uint8_t* byte_parities, uint8_t even, uint32_t div_size, uint32_t chunk_size)
{
    uint8_t result = 0;
    int32_t i, j;

    if(byte_parities == NULL)
        return -1;

    for (i = (even ? 0 : div_size); i < chunk_size; i += (2 * div_size)) {
        for (j = 0; j < div_size; j++) {
            result ^= byte_parities[i + j];
        }
    }
    return result & 0x1;
}

// Calculates the ECC for a given set of data
// Can be sized to suit the size of the data
/* Based on Texas Instrument's C# GenECC application
   (sourceforge.net/projects/dvflashutils) */
static uint32_t calc_ecc(uint8_t *buf, uint32_t chunk_size)
{
    uint16_t odd_result = 0, even_result = 0;
    uint8_t bit_parities = 0;
    uint8_t byte_parities[chunk_size];
    int32_t i;
    uint8_t val;

    if(buf == NULL)
        return -1;

    for (i = 0; i < chunk_size; ++i) {
        bit_parities ^= buf[i];
    }

    // Bitwise parities
    even_result |= ((calc_bitwise_parity(bit_parities, EVEN_HALF) << 2) |
                   (calc_bitwise_parity(bit_parities, EVEN_FOURTH) << 1) |
                   (calc_bitwise_parity(bit_parities, EVEN_EIGHTH) << 0));

    odd_result |= ((calc_bitwise_parity(bit_parities, ODD_HALF) << 2) |
                  (calc_bitwise_parity(bit_parities, ODD_FOURTH) << 1) |
                  (calc_bitwise_parity(bit_parities, ODD_EIGHTH) << 0));

    // Bytewise parities
    for (i = 0; i < chunk_size; ++i) {
        byte_parities[i] = calc_bitwise_parity(buf[i], EVEN_WHOLE);
    }

    for (i = 0; i < LOG2(chunk_size); ++i) {
        val = 0;
        val = calc_row_parity_bits(byte_parities, 1, 1 << i, chunk_size);
        even_result |= (val << (3 + i));

        val = calc_row_parity_bits(byte_parities, 0, 1 << i, chunk_size);
        odd_result |= (val << (3 + i));
    }

    // Shift by 16 bits - each result occupies 'half' of int32 bits
    return (odd_result << 16) | even_result;
}

// Write ECC to the spare area
static int32_t write_ecc(uint32_t block, uint32_t page, uint32_t sector_num, uint8_t* ecc, uint8_t* spare_ecc)
{
    uint32_t spare_size = OOB_SIZE * OOB_SECTORS;
    uint32_t sector_addr = sector_num * OOB_SIZE;
    uint8_t sector[spare_size];
    uint32_t i, j = 0;

    if(ecc == NULL || spare_ecc == NULL)
        return -1;

    for(i = 0; i < spare_size; ++i)
        sector[i] = 0xff;
    // Add sector ECC
    for(i = sector_addr + SECTOR_ECCADDR; i < sector_addr + SECTOR_ECCADDR + SECTOR_ECCLEN; ++i) {
        sector[i] = ecc[j];
        ++j;
    }
    // Add spare ECC
    j = 0;
    for(i = sector_addr + SPARE_ECCADDR; i < sector_addr + SPARE_ECCADDR + SPARE_ECCLEN; ++i) {
        sector[i] = spare_ecc[j];
        ++j;
    }

    return oob_write(block, page, &sector, spare_size);
}

// Calculate ECC for sector
static uint32_t calc_sector_ecc(uint8_t* sector, uint8_t* code)
{
    uint8_t *p;
    int32_t ecc = 0;

    if(sector == NULL)
        return -1;

    ecc = calc_ecc(sector, SECTORSIZE);

    // Parity calculation
    p = (uint8_t*)&ecc;

    code[0] = p[0];
    code[1] = p[1] | (p[3] << 4);
    code[2] = p[2];

    return *((uint32_t*)code) >> 8;
}

// Calculates the ECC for the spare area
static uint16_t calc_spare_ecc(uint8_t* spare, uint8_t* code)
{
    uint8_t *p;
    int32_t ecc;

    if(spare == NULL)
        return -1;

    ecc = calc_ecc(spare, SECTOR_ECCLEN);

    // Parity calculation
    p = (uint8_t*)&ecc;

    code[0] = p[0] | ((p[2] & ODD_HALF) << 1) | ((p[2] & EVEN_HALF) & 0xc);
    code[1] = ((p[2] & EVEN_HALF) & 0x3);

    return *((uint16_t*)code);
}

// Flips the bit at the specified location in a given sector
static int32_t perform_bit_correction(uint8_t* sector, uint32_t byte, uint32_t bit)
{
    uint32_t bit_mask;

    if(sector == NULL)
        return -1;

    bit_mask = 1 << bit;
    sector[byte] ^= bit_mask;

    return bit_mask;
}


// Checks for correctable single bit errors, returns an error if there is more
// than a single bit error
static int32_t compare_ecc(uint8_t* data, uint8_t* oob_ecc, uint8_t* read_ecc, uint8_t spare)
{
    uint32_t correctable, odd_mask, even_mask, shift;
    if(spare != 0) {
        correctable = SPARE_CORRECTABLE;
        odd_mask = SPARE_ODD_MASK;
        even_mask = SPARE_EVEN_MASK;
        shift = 5;
    } else {
        correctable = SECT_CORRECTABLE;
        odd_mask = SECT_ODD_MASK;
        even_mask = SECT_EVEN_MASK;
        shift = SECTOR_ECCLEN * 4;
    }

    uint32_t old_ecc = *((uint32_t*) oob_ecc) & correctable;
    uint32_t new_ecc = *((uint32_t*) read_ecc) & correctable;
    uint32_t odd = ((old_ecc & odd_mask) ^ (new_ecc & odd_mask)) >> shift;
    uint32_t even = (old_ecc & even_mask) ^ (new_ecc & even_mask);

    if ((odd ^ even) != ECC_MATCH){
        // If error not correctable
        if((odd ^ even) != correctable)
            return TWO_BIT_CORRUPTION;

        perform_bit_correction(data, odd >> BYTE_HALF_POS, odd & BIT_HALF_MASK);
        return ECC_CORRECTION_MADE;
    }

    return ECC_MATCH;
}

// Extracts just the ECC written into a sector's OOB
// Variable, depending on the size of the code
// Returns an integer version of the code
static uint32_t read_sector_ecc(uint8_t* sect_oob, uint8_t* sect_ecc, uint8_t* spare_ecc)
{
    uint8_t i = 0, j = 0;
    uint8_t length = SECTOR_ECCADDR + SECTOR_ECCLEN;

    // Read ECC for sector
    for(i = 0, j = SECTOR_ECCADDR; j < length; ++j, ++i) {
        sect_ecc[i] = sect_oob[j];
    }

    // Read ECC for sector's spare area
    /*for(i = 0, j = SPARE_ECCADDR; j < SPARE_ECCADDR + SPARE_ECCLEN; ++j, ++i) {
        spare_ecc[i] = sect_oob[j];
    }*/

    return *((uint32_t*)sect_ecc) >> 8;
}

// Calculate ECC for each sector of a written page
int32_t write_page_ecc(uint32_t block_num, uint32_t page_num, uint8_t* page)
{
    uint8_t sect_code[SECTOR_ECCLEN];
    uint8_t spare_code[SPARE_ECCLEN];
    uint8_t ec;
    uint32_t i;

    if(page == NULL || page == NULL)
        return -1;

    for (i = 0; i < PAGESIZE / SECTORSIZE; ++i) {
        // Obtain ECC code for each sector and write to respective OOB
        calc_sector_ecc(page + i * SECTORSIZE, sect_code);
        calc_spare_ecc(sect_code, spare_code);
        if((ec = write_ecc(block_num, page_num, i, sect_code, spare_code)) < 0)
            return ec;
    }

    return 0;
}

// Calculate ECC for page to be read and compare to current ECC in it's OOB
// Performed sector-by-sector
// Redundant ECC generation for the sector ECC has been commented out
int32_t read_page_ecc(void** buf, uint32_t block_num, uint32_t page_num)
{
    uint8_t calc_ecc[SECTOR_ECCLEN]; // New ECC for sector
    uint8_t sect_ecc[SECTOR_ECCLEN]; // Old ECC for sector
    uint8_t spare_ecc[SPARE_ECCLEN]; // Old ECC for spare area
    uint8_t oob[OOB_SIZE];
    void* oob_buf = oob;
    void* page = NULL;
    int8_t ec;

    if(buf == NULL || *buf == NULL)
        return -1;

    page = *buf;

    if ((ec = oob_read(block_num, page_num, &oob_buf)) < 0)
        return ec;

    for (uint32_t i = 0; i < PAGESIZE / SECTORSIZE; ++i) {
        calc_sector_ecc((uint8_t*)page + i * SECTORSIZE, calc_ecc);
        // calc_spare_ecc(calc_ecc, calc_spare);
        read_sector_ecc((uint8_t*)oob_buf + i * OOB_SIZE, sect_ecc, spare_ecc);
        // compare_ecc(sect_ecc, calc_spare, spare_ecc, 1);
        compare_ecc((uint8_t*)page + i * SECTORSIZE, calc_ecc, sect_ecc, 0);
    }

    *buf = page;
    free(oob_buf);

    return 0;
}
