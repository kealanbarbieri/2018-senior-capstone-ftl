#ifndef NAND_H
#define NAND_H

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define addr_to_page(A) (A / PAGESIZE)
#define addr_byte_offset(A) (A % PAGESIZE)
#define page_to_addr(A) (A * PAGESIZE)

/* The first argument is the eraseblock you want to write to i.e.,
 *  if you have five erase blocks labelled 0 to 4, passing 1 will
 *  get you the second erase block
 *
 *  Page is a number into the eraseblock. For example, if the eraseblock
 *   you chose has ten erase blocks arranged 0 to 10, passing in 2 will
 *   write to the third page
 *
 * The input is the data you want to write
 *
 * Inputlen is the lenth of the input. I usually use strlen to figure it out
 *
 * Return value is page size if successful, an error code from src/common.h otherwise
 */
uint32_t page_write(uint32_t eraseblock, uint32_t page, void * input, size_t inputlen);
/*
 * The first argument is the eraseblock you want to write to i.e.,
 *  if you have five erase blocks labelled 0 to 4, passing 1 will
 *  get you the second erase block
 *
 *  Page is a number into the eraseblock. For example, if the eraseblock
 *   you chose has ten erase blocks arranged 0 to 10, passing in 2 will
 *   write to the third page
 *
 *  buf is a pointer to a pointer. *buf is where the returned data will
 *  be written to. The extra layer of indirection is to make it page size
 *  agnostic.
 *
 *  The Return value will be page size if success, and an error code
 *  from common.h otherwise
 *
 * IMPORTANT NOTE: New Memory is allocatated at *buf. If *buf is
 *  already pointing at something, it will be lost. *buf will have to be
 * 	freed at some point. This is to make the function pagesize agnostic
 *
*/
uint32_t page_read(uint32_t eraseblock, uint32_t page, void ** buf);
/*
 * The first argument is the eraseblock you want to write to i.e.,
 *  if you have five erase blocks labelled 0 to 4, passing 1 will
 *  get you the second erase block
 *
 *  Page is a number into the eraseblock. For example, if the eraseblock
 *   you chose has ten erase blocks arranged 0 to 10, passing in 2 will
 *   write to the third page
 *
 *  buf is a pointer to a pointer. *buf is where the returned data will
 *  be written to. The extra layer of indirection is to make it OOB size
 *  agnostic.
 *
 * The return value will be OOB size if success, and an error code from s
 *  src/common.h otherwise
 */
uint32_t oob_read(uint32_t eraseblock, uint32_t page, void ** buf);
/*
 * The first argument is the eraseblock you want to write to i.e.,
 *  if you have five erase blocks labelled 0 to 4, passing 1 will
 *  get you the second erase block
 *
 *  Page is a number into the eraseblock. For example, if the eraseblock
 *   you chose has ten erase blocks arranged 0 to 10, passing in 2 will
 *   write to the third page
 *
 *  buf is a pointer to a pointer. *buf is where the returned data will
 *  be written to. The extra layer of indirection is to make it page size
 *  agnostic.
 *
 *  The Return value will be the oob size if success and an error value
 *  from src/common.h otherwise
 *
 * IMPORTANT NOTE: New Memory is allocatated at *buf. If *buf is
 *  already pointing at something, it will be lost. *buf will have to be
 * 	freed at some point. This is to make the function oob size agnostic
 *
 */
uint32_t oob_write(uint32_t eraseblock, uint32_t page, void * buf, size_t inputlen);
/* Just pass in which eraseblock you'd like to erase.
 *
 *  If success, function returns 0. Otherwise it returns an error from 
 *  src/common.h
 */
uint32_t _erase(uint32_t eraseblock);

#endif
