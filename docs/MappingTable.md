
## Mapping Table

The mapping table is an in memory structure that provides a direct map between logical page numbers and physical blocks in flash memory. 

Logical page numbers modified to find the corresponding logical block, and that block is used as an index into the table. Logical page numbers are what the user interacts with, and due to the abstraction of the mapping table, are always contiguous. Mapped physical blocks are offsets into the underlying NAND flash device. 

In memory, the table is represented as an array of `table_entry` structures, and contains its size:

```
// An entry of the mapping table
typedef struct {
    uint32_t phys_block;   // The block offset data lives in on NAND flash
    uint32_t erase_count;  // How many times the block has been erased
    boolean valid;         // Whether the given block is valid for garbage collection
} table_entry;

// The mapping table
typedef struct {
    table_entry **entries; // Array of entries
    uint32_t size;         // Size of entry array
} mapping_table;
```

The mapping table provides the following features, described in `src/mapping_table.h`:

* map logical pages to physical blocks
* set valid bits of physical blocks
* flush the mapping table to NAND flash
* free the mapping table from memory
* swap two entries in the mapping table
* serialize a table entry
* deserialize a table entry
* serialize a page of entries in the mapping talbe
* initialize a mapping table from serialized entries located on NAND flash 

