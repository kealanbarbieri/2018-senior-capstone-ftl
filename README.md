# 2018 Senior Capstone FTL

## FTL Capstone Project
The purpose of this piece of software is to make a File Translation Layer to work within simulation, specifically nandsim. But can be modified to work within actual hardware and have a filesystem attached. This was created with the intention to work within AltOS, which has shared memory, so further modification will be needed to meet any other requirement or specification. This was created to be more like a library than a seperate process, because of the shared memory within AltOS. The assumption is that there will be sequential writes to the nand storage, as such there may be performance hits in random write use cases. It was created to be used on 32-bit hardware with very limited resources. As such, the dependencies should have 32-bit versions installed in order build and test. The storage overhead is 33% in order to store the tables for mapping, bad blocks, and garbage collection on the device. There is dynamic wear leveling, it is implemented by the garbage collector.


## Dependencies (apt-packages)
For make build
- sudo (if machine doesn't come with it preinstalled)
- make
- cmake
- build-essential
- gcc-multilib


For make test, in addition to make build
May need to run dpkg --add-architecture i386 if apt cannot find the following packages
- libcmocka0:i386
- libcmocka-dev:i386
- mtd-utils

## Installing
IMPORTANT NOTE: Software assumes nandsim device is installed at /dev/mtd0. If something
else is there, you might have a bad day.


In order to install all dependencies in one go, run this command (may require sudo):

```
dpkg --add-architecture i386 && \
apt-get update -y && \
apt-get upgrade -y && \
apt-get install -y make && \
apt-get install -y cmake && \
apt-get install -y build-essential && \
apt-get install -y gcc-multilib && \
apt-get install -y libcmocka0:i386 && \
apt-get install -y libcmocka-dev:i386 && \
apt-get install -y mtd-utils && \
apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
```


To setup the device to be tested, there needs to be a virtual nand device to run, testing will be done on nandism, to do this run:
```
sudo modprobe nandsim first_id_byte=0x20 second_id_byte=0x78
```
Verify that the configuration looks like the by using 
```
mtdinfo -a
```
By running that command, you should get the following output:
```
Count of MTD devices:           1
Present MTD devices:            mtd0
Sysfs interface supported:      yes

mtd0
Name:                           NAND simulator partition 0
Type:                           nand
Eraseblock size:                16384 bytes, 16.0 KiB
Amount of eraseblocks:          8192 (134217728 bytes, 128.0 MiB)
Minimum input/output unit size: 512 bytes
Sub-page size:                  256 bytes
OOB size:                       16 bytes
Character device major/minor:   90:0
Bad blocks are allowed:         true
Device is writable:             true
```

From here the program should be able to build and test successfully. If mtdinfo -a does not output the same as shown above, you may need to retry and make sure that you entered the correct parameters into nandsim.

## Testing
Make test should give the following output(should output more �'s than shown, cleaned it up to not not have the horizontal scrollbar):
```
test/mapping_table_tests
[==========] Running 9 test(s).
[ RUN      ] test_logical_to_physical
[       OK ] test_logical_to_physical
[ RUN      ] test_init_mapping_table_dev
[       OK ] test_init_mapping_table_dev
[ RUN      ] test_flush_table_dev
[       OK ] test_flush_table_dev
[ RUN      ] test_free_table_dev
[       OK ] test_free_table_dev
[ RUN      ] test_table_swap
[       OK ] test_table_swap
[ RUN      ] test_translation_macros
[       OK ] test_translation_macros
[ RUN      ] test_entry_serialization
[       OK ] test_entry_serialization
[ RUN      ] test_table_serialization
[       OK ] test_table_serialization
[ RUN      ] test_set_valid_bit
[       OK ] test_set_valid_bit
[==========] 9 test(s) run.
[  PASSED  ] 9 test(s).

 0 FAILED TEST(S)
test/gc_tests
[==========] Running 7 test(s).
[ RUN      ] test_select_victim
[       OK ] test_select_victim
[ RUN      ] test_select_specific_victim
[       OK ] test_select_specific_victim
[ RUN      ] test_select_victim_fail
[       OK ] test_select_victim_fail
[ RUN      ] test_garbage_collection_success
[       OK ] test_garbage_collection_success
[ RUN      ] test_garbage_collection_no_table
[       OK ] test_garbage_collection_no_table
[ RUN      ] test_garbage_collection_all_valid
[       OK ] test_garbage_collection_all_valid
[ RUN      ] test_gc_with_diff_free_block
[       OK ] test_gc_with_diff_free_block
[==========] 7 test(s) run.
[  PASSED  ] 7 test(s).

 0 FAILED TEST(S)
sudo test/driver_tests
[==========] Running 6 test(s).
[ RUN      ] test_page_write
[       OK ] test_page_write
[ RUN      ] test_page_read
La dee la la�����������������������������������������������������������������������
[       OK ] test_page_read
[ RUN      ] test_oob_write
[       OK ] test_oob_write
[ RUN      ] test_oob_read
[       OK ] test_oob_read
[ RUN      ] test_manual_write_check
[       OK ] test_manual_write_check
[ RUN      ] test_erase
[       OK ] test_erase
[==========] 6 test(s) run.
[  PASSED  ] 6 test(s).

 0 FAILED TEST(S)
```
Make system_tests should output:
```
sudo test/system_tests
[==========] Running 1 test(s).
[ RUN      ] test_basic_rw
[       OK ] test_basic_rw
[==========] 1 test(s) run.
[  PASSED  ] 1 test(s).

 0 FAILED TEST(S)
#sudo flash_eraseall /dev/mtd0
#sudo test/sequential_write
``` 

There may be extra output errors in addition to the tests due to the imported read/write interface that was pulled from the Linux kernal.

## Authors
- River Bartz - [@riverbartz](https://gitlab.com/riverbartz)
- Alexis Murauskas - [@alexis.murauskas](https://gitlab.com/alexis.murauskas)
- Anders Swanson - [@anders.swanson](https://gitlab.com/anders.swanson)
- William Haugen - [@PieMyth](https://gitlab.com/PieMyth)
- Joe Christman - [@jchristm77](https://gitlab.com/jchristm77)
- Jason Larson - [@JasonLarson44](https://gitlab.com/JasonLarson44)
- Kealan Barbieri - [@kealanbarbieri](https://gitlab.com/kealanbarbieri)

## License
This piece of software is licensed under the GNU General Public License v2.0 see the [LICENSE.md](../LICENSE) for more details
