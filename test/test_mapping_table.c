#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../src/mapping_table.h"
#include "../src/mapping_table.c"

#define PBLOCK 100
#define ECOUNT 99
#define EVALID true
#define MOCK_FTL_SIZE (PAGESIZE * 5)

char *mock_ftl;

uint32_t page_write(uint32_t eraseblock, uint32_t page, void *buf, size_t inputlen) {
    uint32_t offset = (eraseblock * PAGES_PER_BLOCK) + (page * PAGESIZE);
    memcpy(mock_ftl+offset, buf, inputlen);
    return true;
}

uint32_t _erase(uint32_t eraseblock) {
    return true;
}

uint32_t page_read(uint32_t eraseblock, uint32_t page, void **buf) {
    uint32_t p_offset = PAGESIZE * page;
    uint32_t b_offset = eraseblock * PAGES_PER_BLOCK * PAGESIZE;
    buf[0] = malloc(PAGESIZE);
    memcpy(buf[0], mock_ftl+p_offset+b_offset, PAGESIZE);
}

static void test_translation_macros(void **state) {
    // run each translation macro and verify the results
    uint32_t addr1 = (PAGESIZE * 5) + 3;
    uint32_t addr2 = PAGESIZE * 5;

    // it should allow translation of a byte address to a page number
    assert_int_equal(addr_to_page(addr1), 5);
    // it should get the byte offset into a page from a byte address
    assert_int_equal(addr_byte_offset(addr1), 3);
    // it should allow translation of a page number to a byte address
    assert_int_equal(page_to_addr(5), addr2);
}

static void test_init_mapping_table_dev(void **state) {
    // it should initialize a direct mapped table of memory blocks
    mapping_table *table = init_mapping_table(DUMMY_TABLE, DEV_TABLE_SIZE);

    for (uint32_t i = 0; i < DEV_TABLE_SIZE; ++i) {
        assert_int_equal(table->entries[i]->phys_block, i);
        assert_int_equal(table->entries[i]->erase_count, 0);
        assert_int_equal(table->entries[i]->valid, true);
    }
    free_table(table);
}

static void test_logical_to_physical(void **state) {
    mapping_table *table = NULL;
    ftl_addr * addr = NULL;

    // it should not allow translation on an uninitialized table
    assert_null(logical_to_physical(0, table));

    table = init_mapping_table(DUMMY_TABLE, DEV_TABLE_SIZE);

    // it should not allow translation for a block outside the table range
    // (table_size < (lpn / PAGES_PER_BLOCK)
    assert_null(logical_to_physical(PAGES_PER_BLOCK * (DEV_TABLE_SIZE+1), table));

    // it should translate logical page numbers to physical block numbers
    // with a page offset

    addr = logical_to_physical(99, table);
    assert_int_equal(addr->block, 3);
    if(addr) {
         free(addr);
    }

    addr = logical_to_physical(99, table);
    assert_int_equal(addr->page_offset, 3);
    if(addr) {
        free(addr);
    }

    free_table(table);
}

static void test_table_serialization(void **state) {
    mapping_table *table1 = init_mapping_table(DUMMY_TABLE, DEV_TABLE_SIZE);
    mapping_table *table2 = NULL;
    uint32_t size = ENTRY_SIZE;
    uint32_t off = 0;

    // it should not allow serialization of null tables
    assert_null(serialize_te_page(table2, 0));

    // it should allow serialization of the mapping table into page-size chunks
    mock_ftl = malloc(MOCK_FTL_SIZE);

    char *buf = serialize_te_page(table1, 0);
    memcpy(mock_ftl, buf, PAGESIZE);
    free(buf);

    buf = serialize_te_page(table1, ENTRIES_PER_PAGE);
    memcpy(mock_ftl+PAGESIZE, buf, PAGESIZE);
    free(buf);

    buf = serialize_te_page(table1, ENTRIES_PER_PAGE * 2);
    memcpy(mock_ftl+(PAGESIZE*2), buf, PAGESIZE);
    free(buf);

    buf = serialize_te_page(table1, ENTRIES_PER_PAGE * 3);
    memcpy(mock_ftl+(PAGESIZE*3), buf, size * 4);
    free(buf);

    // it should allow deserialization of serialized tables
    table2 = init_mapping_table(0, DEV_TABLE_SIZE);
    for (uint32_t i = 0; i < DEV_TABLE_SIZE; ++i) {
        assert_int_equal(table2->entries[i]->phys_block, table1->entries[i]->phys_block);
        assert_int_equal(table2->entries[i]->erase_count, table1->entries[i]->erase_count);
        assert_int_equal(table2->entries[i]->valid, table1->entries[i]->valid);
    }
    free_table(table1);
    free_table(table2);
    free(mock_ftl);
    mock_ftl = NULL;
}

static void test_flush_table_dev(void **state) {
    mapping_table *table1 = NULL;
    mapping_table *table2 = NULL;
    mock_ftl = malloc(MOCK_FTL_SIZE);

    // it should not allow flushing of null tables
    assert_int_equal(flush_table(table1, 0, 0), -1);

    table1 = init_mapping_table(DUMMY_TABLE, DEV_TABLE_SIZE);

    // it should allow flushing of the mapping table to disk
    assert_int_equal(flush_table(table1, 0, 0), 0);

    // it should allow flushed tables to be reinitialized
    table2 = init_mapping_table(0, DEV_TABLE_SIZE);
    assert_non_null(table2);
    for (uint32_t i = 0; i < DEV_TABLE_SIZE; ++i) {
        assert_int_equal(table2->entries[i]->phys_block, table1->entries[i]->phys_block);
        assert_int_equal(table2->entries[i]->erase_count, table1->entries[i]->erase_count);
        assert_int_equal(table2->entries[i]->valid, table1->entries[i]->valid);
    }
    free_table(table1);
    free_table(table2);
    free(mock_ftl);
    mock_ftl = NULL;
}

static void test_free_table_dev(void **state) {
    mapping_table *table = NULL;

    // it should not allow freeing of a null table
    assert_int_equal(free_table(table), -1);
    table = init_mapping_table(DUMMY_TABLE, DEV_TABLE_SIZE);
    // it should allow the freeing of a mapping table
    assert_int_equal(free_table(table), 0);
}

static void test_table_swap(void **state) {
    mapping_table *table = NULL;
    table_entry entry0;
    table_entry entry1;
    // it should not allow swap on an uninitialized table
    assert_int_equal(swap_table_entry(table, 0, 1), -1);

    table = init_mapping_table(DUMMY_TABLE, NUM_OF_BLOCKS);
    // it should not allow swaps on the mapping table reserved space
    assert_int_equal(swap_table_entry(table, 0, 1), -1);

    // it should allow swapping table entry i with table entry j
    entry0.phys_block = table->entries[NUM_OF_BLOCKS - 2]->phys_block;
    entry1.phys_block = table->entries[NUM_OF_BLOCKS - 1]->phys_block;

    assert_int_equal(swap_table_entry(table, NUM_OF_BLOCKS - 2, NUM_OF_BLOCKS - 1), 0);
    assert_int_equal(table->entries[NUM_OF_BLOCKS - 2]->phys_block,
                     entry1.phys_block);

    assert_int_equal(table->entries[NUM_OF_BLOCKS - 1]->phys_block,
                     entry0.phys_block);
    free_table(table);
}

static void test_entry_serialization(void **state) {
    table_entry *entry = malloc(sizeof(table_entry));
    table_entry *deserialized_entry = NULL;
    char *buf = NULL;

    // it should not allow serialization of null entries
    assert_null(serialize_te(deserialized_entry));

    // it should allow serialization of table entries
    entry->phys_block = PBLOCK;
    entry->erase_count = ECOUNT;
    entry->valid = EVALID;
    buf = serialize_te(entry);
    assert_non_null(buf);
    free(buf);

    // it should allow deserialization of table entries
    deserialized_entry = deserialize_te(entry);
    assert_false(deserialized_entry == NULL);
    assert_int_equal(deserialized_entry->phys_block, PBLOCK);
    assert_int_equal(deserialized_entry->erase_count, ECOUNT);
    assert_int_equal(deserialized_entry->valid, EVALID);

    free(entry);
    free(deserialized_entry);
}

static void test_set_valid_bit(void **state) {
    mapping_table *table = NULL;
    // it should not allow valid bit setting on a null table
    assert_int_equal(set_valid_bit(table, 0, false), -1);

    table = init_mapping_table(DUMMY_TABLE, DEV_TABLE_SIZE);

    boolean invalid = false;

    // it should allow the setting of valid bits for the block of a logical page
    assert_int_equal(table->entries[0]->valid, true);
    assert_int_equal(set_valid_bit(table, 0, invalid), 0);
    assert_int_equal(table->entries[0]->valid, invalid);
    free_table(table);
}

int main(void) {
    const UnitTest tests[] = {
        unit_test(test_logical_to_physical),
        unit_test(test_init_mapping_table_dev),
        unit_test(test_flush_table_dev),
        unit_test(test_free_table_dev),
        unit_test(test_table_swap),
        unit_test(test_translation_macros),
        unit_test(test_entry_serialization),
        unit_test(test_table_serialization),
        unit_test(test_set_valid_bit),
    };
    return run_tests(tests);
}
