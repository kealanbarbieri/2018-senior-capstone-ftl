#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../src/ftl_public.h"

#define TEST_FTL_POSITION 99
#define TEST_DATA "ONCE UPON A TIME"
#define TEST_DATA_SIZE strlen(TEST_DATA)


//write test data to device, ensure correct # of bytes reported written
static void test_ftl_write(void **state) {
	uint8_t buf[OOB_SIZE], *page_buf, buf512[PAGESIZE];
	uint64_t lpn = (RESERVED_AREA_BLOCKS-1)* PAGES_PER_BLOCK, result;

	char page_data[PAGESIZE], big_data[2 * PAGESIZE];

	void * bufptr = buf;
	char* test_data = TEST_DATA;

    memset(bufptr, 0x0, OOB_SIZE);
	memset((void*)page_data, 3, PAGESIZE);
	memset((void*)big_data, 3, 2 *PAGESIZE);

	/*test oobwrite/read
	oob_write(342, 0, bufptr, OOB_SIZE);
	oob_read(342, 0, &bufptr);
	assert_int_equal(strncmp(buf, (char*)bufptr, OOB_SIZE), 0);
	*/

	ftl_addr * paddr = logical_to_physical(lpn, M_TABLE);

	//test < page write unaligned
	result = ftl_write(TEST_FTL_POSITION, test_data, TEST_DATA_SIZE);
	assert_int_equal(result, strlen(test_data));
	result = ftl_read(TEST_FTL_POSITION, bufptr, TEST_DATA_SIZE);
	assert_int_equal(strncmp((char*)bufptr, test_data, TEST_DATA_SIZE), 0);

	//test double write page fail
	result = ftl_write(TEST_FTL_POSITION, test_data, TEST_DATA_SIZE);
	assert_int_equal(result, INVALID_ADDRESS);

	//test page write aligned
	result = ftl_write(PAGESIZE * 3, page_data, PAGESIZE);
	assert_int_equal(result, PAGESIZE);
	page_buf=buf512;
	result = ftl_read(PAGESIZE*3, page_buf, PAGESIZE);
	assert_int_equal(result, PAGESIZE);
	assert_int_equal(strncmp((char*)page_buf, page_data, PAGESIZE), 0);

	//test page write unaligned
	result = ftl_write((PAGESIZE * 4) + 15, page_data, PAGESIZE);
	assert_int_equal(result, PAGESIZE - 15);
	result =  ftl_read((PAGESIZE*4) + 15, page_buf, PAGESIZE);
	assert_int_equal(result, PAGESIZE - 15);
	assert_int_equal(strncmp((char*)page_buf, page_data, PAGESIZE), 0);

	//test write invalid block
	set_valid_bit(M_TABLE, lpn + PAGES_PER_BLOCK, false);
	result = ftl_write(TEST_FTL_POSITION + (BLOCKSIZE), TEST_DATA, TEST_DATA_SIZE);
	assert_int_equal(result, INVALID_ADDRESS);
	set_valid_bit(M_TABLE, lpn + PAGES_PER_BLOCK, true);

}

//read data written by write test, ensure correctness, correct byte count
static void test_ftl_read(void **state) {
	char* test_data = TEST_DATA;
	char buf [TEST_DATA_SIZE];
	void * test_buf = buf;

	int32_t result = ftl_read(TEST_FTL_POSITION, test_buf, TEST_DATA_SIZE);
	assert_int_equal(result, TEST_DATA_SIZE);
	assert_int_equal(strncmp(TEST_DATA, test_buf, TEST_DATA_SIZE), true);

}

//mark page written to in write test for erasing, verify
static void test_ftl_erase(void **state) {

	char readbuf[PAGESIZE];
	void * buf = readbuf;

	//write to second page in 1st block
	int32_t result = ftl_write(TEST_FTL_POSITION + (PAGESIZE), TEST_DATA, TEST_DATA_SIZE);
	assert_int_equal(result, TEST_DATA_SIZE);

	//delete first page in first block
	result = ftl_erase(TEST_FTL_POSITION, TEST_DATA_SIZE);
	assert_int_equal(result, PAGESIZE);

	//check redirect of deleted block preserves second page
	result = ftl_read(TEST_FTL_POSITION + (PAGESIZE), buf, TEST_DATA_SIZE);
	assert_int_equal(result, TEST_DATA_SIZE);
	assert_int_equal(strncmp((char*)buf, TEST_DATA, TEST_DATA_SIZE), 0);

	//test finding a clean block in reserved area (gc)
	mapping_table* table = (mapping_table*)M_TABLE;
	for(int i = FIRST_DATA_BLOCK+1; i < table->size; ++i){
		table->entries[i]->dirty=true;
	}

	result = ftl_write(TEST_FTL_POSITION + (2*PAGESIZE), TEST_DATA, TEST_DATA_SIZE);
	assert_int_equal(result, TEST_DATA_SIZE);

	result = ftl_erase(TEST_FTL_POSITION + (PAGESIZE), TEST_DATA_SIZE);
	assert_int_equal(result, PAGESIZE);

	//check redirect of deleted block preserves third page
	result = ftl_read(TEST_FTL_POSITION + (2*PAGESIZE), buf, TEST_DATA_SIZE);
	assert_int_equal(result, TEST_DATA_SIZE);
	assert_int_equal(strncmp((char*)buf, TEST_DATA, TEST_DATA_SIZE), 0);

	// Check that gc kicks in and frees a block from reserved space
	for(int i = FIRST_DATA_BLOCK+1; i < table->size; ++i){
		table->entries[i]->dirty=true;
	}
	result = ftl_erase(TEST_FTL_POSITION + (PAGESIZE), TEST_DATA_SIZE);
	assert_int_equal(result, PAGESIZE);

	//test error code for no clean blocks remaining (gc)
	for(int i = FIRST_DATA_BLOCK+1; i < table->size; ++i){
		table->entries[i]->dirty=true;
	}
	for(int i = GC_BLOCKS + MTABLE_BLOCKS; i < FIRST_DATA_BLOCK; ++i){
		table->entries[i]->dirty=true;
	}
	result = ftl_erase(TEST_FTL_POSITION + (PAGESIZE), TEST_DATA_SIZE);
	assert_int_equal(result, OUT_OF_RESERVED_BLOCKS);


}

// TODO: Update this test once we get a read function
// We need to write and then read to a mock device.
// Erase it and then try to garbage collect on it
static void test_ftl_gc(void **state) {
	char* test_data = TEST_DATA;
	int32_t result = ftl_gc(M_TABLE);
	// Going to return false if there are no invalid blocks
	assert_int_equal(result, true);
}

int main(void) {
	ftl_init();
    const UnitTest tests[] = {
        unit_test(test_ftl_write),
        unit_test(test_ftl_read),
        unit_test(test_ftl_erase),
        //unit_test(test_ftl_gc),
    };
    double ret = run_tests(tests);
	ftl_free();
	return ret;
}
