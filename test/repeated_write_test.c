#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../src/ftl_public.h"

static void test_repeated_write(void ** state)
{
	int i = 0;
	char data = 'a';
	/*
	   The number 1070 was selected because on linux if too many files
	   get opened it shuts down.
	*/

	for(i = 0; i < 1070; ++i) { /* Number is arbitary. */
		assert_int_equal(ftl_write(0, (void *)&data, 1), 1);
		assert_int_equal(ftl_erase(0, 1), PAGESIZE);
		printf("%d\n", i);
	}

}

int main(void)
{
    //init the shared structures
    ftl_init();
    const UnitTest tests[] =
    {
        unit_test(test_repeated_write),
    };
    return run_tests(tests);
}

