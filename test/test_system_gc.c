#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../src/ftl_public.h"


static void test_gc_no_invalid_blocks(void **state)
{
    // Should fail because there are no invalid blocks yet
    assert_int_equal(ftl_gc(), -1);
}

static void test_gc_manual(void **state)
{
    unsigned char data[4] = {0xaa, 0xbb, 0xcc, 0xdd};
    size_t length = 4;
    uint32_t pageNum = 1;
    uint32_t blockNum = 1;
    uint32_t lpn = (PAGES_PER_BLOCK * blockNum) + pageNum;
    unsigned char ret_data[4];

    uint32_t result = ftl_write(lpn, data, length);
    assert_int_equal(result, length);

    // Now that we wrote the data, let's erase it
    assert_int_equal(ftl_erase(lpn, length), PAGESIZE);
    // Should succeed because the erase call will have marked a block invalid
    assert_int_equal(ftl_gc(), 0);
}

static void test_auto_gc_erase(void **state)
{
    unsigned char data[4] = {0xaa, 0xbb, 0xcc, 0xdd};
    unsigned char ret_data[4];
    uint32_t result = 0;
    size_t length = 4;
    uint32_t pageNum = 0;
    uint32_t blockNum = 0;
    uint32_t lpn = 0;

    // Write to block 0 page 0 so we can erase it
    result = ftl_write(lpn, data, length);
    assert_int_equal(result, length);

    // Now it's marked as invalid so that gc can clean it up
    assert_int_equal(ftl_erase(lpn, length), PAGESIZE);

    // Write to every other block
    for(int32_t i = 1; i < USER_DATA_BLOCKS; ++i){
        blockNum = i;
        lpn = (PAGES_PER_BLOCK * blockNum) + pageNum;
        result = ftl_write(lpn, data, length);
    }

    // Try to erase a block other than the first one when no free blocks
    // Should trigger gc to free up a block
    assert_int_equal(ftl_erase(PAGES_PER_BLOCK*2, length), PAGESIZE);

}

int main(void)
{
    //init the shared structures
    ftl_init();
    const UnitTest tests[] =
    {
        // Run gc test first to ensure no blocks have been marked invalid
        unit_test(test_gc_no_invalid_blocks),
        unit_test(test_gc_manual),
        unit_test(test_auto_gc_erase),
    };
    double ret = run_tests(tests);
    ftl_free();
    return ret;
};
