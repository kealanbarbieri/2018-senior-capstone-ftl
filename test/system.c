#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../src/ftl_public.h"


static void test_basic_rw(void **state)
{
    unsigned char data[4] = {0xaa, 0xbb, 0xcc, 0xdd};
    size_t length = 4;
    uint32_t pageNum = 0;
    uint32_t blockNum = 0;
    uint32_t lpn = (PAGES_PER_BLOCK * blockNum) + pageNum;
    unsigned char ret_data[4];

    uint32_t result = ftl_write(lpn, data, length);
    assert_int_equal(result, 4);
    result = ftl_read(lpn, ret_data, length);
    assert_int_equal(result, 4);

    assert_int_equal(ret_data[0], 0xaa);
    assert_int_equal(ret_data[1], 0xbb);
    assert_int_equal(ret_data[2], 0xcc);
    assert_int_equal(ret_data[3], 0xdd);
}

int main(void)
{
    //init the shared structures
    ftl_init();
    const UnitTest tests[] =
    {
        unit_test(test_basic_rw),
    };
    double ret = run_tests(tests);
    ftl_free();
    return ret;
};
