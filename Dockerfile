FROM jasonlarson44/32-bit-gcc-cmocka

ENV WORK_DIR /opt/capstone-ftl
RUN mkdir -p $WORK_DIR 
COPY src/ $WORK_DIR/src
COPY dist/ $WORK_DIR/dist
COPY test/ $WORK_DIR/test
COPY Makefile $WORK_DIR/

WORKDIR $WORK_DIR
